'''
                                  ESP Health
                         Notifiable Diseases Framework
                         Populate Unmapped Labs Cache


@author: Jason McVetta <jason.mcvetta@gmail.com>
@organization: Channing Laboratory <http://www.channing.harvard.edu>
@copyright: (c) 2009 Channing Laboratory
@license: LGPL 3.0 <http://www.gnu.org/licenses/lgpl-3.0.txt>
'''

from django.core.management.base import BaseCommand

from ESP.emr.management.commands.make_fakes import *



class Command(BaseCommand):
    
    help = 'Extracts ESP data to Epic ETL files.'
    
    @transaction.atomic
    def handle(self, *fixture_labels, **options):
        log.info('Extracting ESP data')
        print 'Extracting ESP data'
        
        #file_path = os.path.join(DATA_DIR, 'fake', self.__class__.filename(d))
        
        count = 0
        provider_writer = ProviderWriter()
         
        for p in Provider.objects.order_by('natural_key').exclude(natural_key__icontains="UNKNOWN"): 
            count =count +1 
            provider_writer.write_row(p)
        log.info('Extracted %s Provider from ESP to Epic ETL files' % count)   
        print 'Extracted %s Provider from ESP to Epic ETL files' % count
                 
        count = 0
        patient_writer = PatientWriter()
        #patient_writer.file_path = file_path
        for p in Patient.objects.order_by('natural_key').exclude(natural_key__icontains="UNKNOWN"): 
            count =count +1 
            patient_writer.write_row(p)
        log.info('Extracted %s Patients from ESP to Epic ETL files' % count)   
        print 'Extracted %s Patients from ESP to Epic ETL files' % count
        
        count = 0  
        lx_writer = LabResultWriter()
        for lx in LabResult.objects.order_by('natural_key'):
            count += 1
            lx_writer.write_row(lx)
        log.info('Extracted %s Labs from ESP to Epic ETL files' % count)  
        print 'Extracted %s Labs from ESP to Epic ETL files' % count
        
        count = 0    
        encounter_writer = EncounterWriter()
        for enc in Encounter.objects.order_by('natural_key'):
            count += 1
            encounter_writer.write_row(enc, enc.dx_codes_str.split(','))
        log.info('Extracted %s Encounters from ESP to Epic ETL files' % count)  
        print 'Extracted %s Encounters from ESP to Epic ETL files' % count
        
        count = 0
        prescription_writer = PrescriptionWriter()
        for meds in Prescription.objects.order_by('natural_key'):
            count += 1
            prescription_writer.write_row(meds)
        log.info('Extracted %s Prescriptions from ESP to Epic ETL files' % count)  
        print 'Extracted %s Prescriptions from ESP to Epic ETL files' % count
        
        count = 0    
        immunization_writer = ImmunizationWriter()
        for imm in Immunization.objects.order_by('natural_key'):
            count += 1
            immunization_writer.write_row(imm)
        log.info('Extracted %s Immunizations from ESP to Epic ETL files' % count)  
        print 'Extracted %s Immunizations from ESP to Epic ETL files' % count
        
        count = 0    
        laborder_writer = LabOrderWriter()
        for lx_order in LabOrder.objects.order_by('natural_key'):
            count += 1
            laborder_writer.write_row(lx_order)
        log.info('Extracted %s Lab Orders from ESP to Epic ETL files' % count)  
        print 'Extracted %s Lab Orders from ESP to Epic ETL files' % count
       
        count = 0    
        allergy_writer = AllergyWriter()
        for allergy in Allergy.objects.order_by('natural_key'):
            count += 1
            allergy_writer.write_row(allergy )
        log.info('Extracted %s Allergies from ESP to Epic ETL files' % count)  
        print 'Extracted %s Allergies from ESP to Epic ETL files' % count
       
        count = 0    
        problem_writer = ProblemWriter()
        for p in Problem.objects.order_by('natural_key'):
            count += 1
            problem_writer.write_row(p)
        log.info('Extracted %s Problem from ESP to Epic ETL files' % count)  
        print 'Extracted %s Problem from ESP to Epic ETL files' % count
        
        count = 0    
        socialhistory_writer = SocialHistoryWriter()
        for p in SocialHistory.objects.order_by('id'):
            count += 1
            socialhistory_writer.write_row(p)
        log.info('Extracted %s social history from ESP to Epic ETL files' % count)  
        print 'Extracted %s social history from ESP to Epic ETL files' % count
        
        count = 0    
        pregnancy_writer = PregnancyWriter()
        for p in Pregnancy.objects.order_by('id'):
            count += 1
            pregnancy_writer.write_row(p)
        log.info('Extracted %s pregnancy from ESP to Epic ETL files' % count)  
        print 'Extracted %s pregancy from ESP to Epic ETL files' % count
