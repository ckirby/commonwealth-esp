#!/usr/bin/python
'''
Rolls through all EMR records, checking that relevant string fields have been
correctly converted to float fields. 
'''

from django.db import transaction
from django.db.models import Q
from django.core.management.base import BaseCommand
from ESP.emr.models import LabResult
from ESP.emr.models import Prescription
from ESP.utils.utils import float_or_none


    
class Command(BaseCommand):
    
    help = 'Ensure float fields in all LabResult and Encounter objects are correctly converted \n'
    help += 'from their corresponding string fields.'
    
    def handle(self, *fixture_labels, **options):
        self.check_labs()
        self.check_prescriptions()
    
    def check_labs(self):
        i = 0
        q_obj = Q(ref_high_string__isnull=False, ref_high_float__isnull=True)
        q_obj |= Q(ref_low_string__isnull=False, ref_low_float__isnull=True)
        q_obj |= Q(result_string__isnull=False, result_float__isnull=True)
        qs = LabResult.objects.filter(q_obj)
        #log_query('Lab results to check for float results', qs)
        tot = qs.count()
        #tot = 0
        for lab in qs.iterator():
            i += 1
            lab.ref_high_float = float_or_none(lab.ref_high_string)
            lab.ref_low_float = float_or_none(lab.ref_low_string)
            lab.result_float = float_or_none(lab.result_string)
            try:
                with transaction.atomic:
                    lab.save()
            except:
                print 'ERROR: Could not process record # %s' % lab.pk
                continue
            print 'Lab  %20s/%s' % (i, tot)

    def check_prescriptions(self):
        i = 0
        qs = Prescription.objects.filter(quantity__isnull=False, quantity_float__isnull=True).order_by('pk')
        tot = qs.count()
        for rx in qs:
            i += 1
            rx.quantity_float = float_or_none(rx.quantity)
            try:
                with transaction.atomic:
                    rx.save()
            except:
                print 'ERROR: Could not process record # %s' % rx.pk
                continue
            print 'Rx  %20s/%s' % (i, tot)
    

    
