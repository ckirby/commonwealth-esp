# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('emr', '0001_initial'),
        ('static', '0001_initial'),
        ('contenttypes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='AdverseEvent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('date', models.DateField()),
                ('gap', models.IntegerField(null=True)),
                ('matching_rule_explain', models.CharField(max_length=200)),
                ('category', models.CharField(max_length=20, choices=[(b'1_rare', b'1_rare'), (b'2_possible', b'2_possible'), (b'3_reportable', b'3_reportable')])),
                ('digest', models.CharField(max_length=200, null=True)),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('last_updated', models.DateTimeField(auto_now=True)),
                ('version', models.CharField(max_length=20, null=True)),
                ('object_id', models.PositiveIntegerField(db_index=True)),
                ('last_known_value', models.FloatField(null=True, verbose_name=b'Last known Numeric Test Result', blank=True)),
                ('last_known_date', models.DateField(null=True, blank=True)),
                ('priority', models.IntegerField(default=3, db_index=True, choices=[(b'3', b'3'), (b'2', b'2'), (b'1', b'1')])),
            ],
            options={
                'ordering': ['id'],
                'permissions': [('view_phi', 'Can view protected health information')],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AllergyEvent',
            fields=[
                ('adverseevent_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='vaers.AdverseEvent')),
            ],
            options={
            },
            bases=('vaers.adverseevent',),
        ),
        migrations.CreateModel(
            name='Case',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(db_index=True)),
                ('last_update', models.DateTimeField(auto_now=True, db_index=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='DiagnosticsEventRule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('category', models.CharField(max_length=60, choices=[(b'1_rare', b'1_rare'), (b'2_possible', b'2_possible'), (b'3_reportable', b'3_reportable')])),
                ('in_use', models.BooleanField(default=False)),
                ('source', models.CharField(max_length=30, null=True)),
                ('ignore_period', models.PositiveIntegerField(null=True)),
                ('risk_period', models.IntegerField(help_text=b'MAX Risk period in days following vaccination')),
                ('risk_period_start', models.IntegerField(default=1, help_text=b'Risk period start day following vaccination')),
                ('heuristic_defining_codes', models.ManyToManyField(related_name=b'defining_dx_code_set', to='static.Dx_code')),
                ('heuristic_discarding_codes', models.ManyToManyField(related_name=b'discarding_dx_code_set', to='static.Dx_code')),
            ],
            options={
                'abstract': False,
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='EncounterEvent',
            fields=[
                ('adverseevent_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='vaers.AdverseEvent')),
            ],
            options={
            },
            bases=('vaers.adverseevent',),
        ),
        migrations.CreateModel(
            name='ExcludedDx_Code',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(unique=True, max_length=20, db_index=True)),
                ('type', models.CharField(max_length=10, verbose_name=b'Code type')),
                ('description', models.CharField(max_length=255)),
            ],
            options={
                'verbose_name': 'Excluded dx code',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='HospProblemEvent',
            fields=[
                ('adverseevent_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='vaers.AdverseEvent')),
            ],
            options={
            },
            bases=('vaers.adverseevent',),
        ),
        migrations.CreateModel(
            name='LabResultEvent',
            fields=[
                ('adverseevent_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='vaers.AdverseEvent')),
            ],
            options={
            },
            bases=('vaers.adverseevent',),
        ),
        migrations.CreateModel(
            name='PrescriptionEvent',
            fields=[
                ('adverseevent_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='vaers.AdverseEvent')),
            ],
            options={
            },
            bases=('vaers.adverseevent',),
        ),
        migrations.CreateModel(
            name='ProblemEvent',
            fields=[
                ('adverseevent_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='vaers.AdverseEvent')),
            ],
            options={
            },
            bases=('vaers.adverseevent',),
        ),
        migrations.CreateModel(
            name='Questionnaire',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comment', models.TextField()),
                ('created_on', models.DateTimeField(auto_now_add=True)),
                ('last_updated', models.DateTimeField(auto_now=True)),
                ('message_ishelpful', models.NullBooleanField()),
                ('interrupts_work', models.NullBooleanField()),
                ('satisfaction_num_msg', models.CharField(max_length=10, db_index=True)),
                ('inbox_message', models.TextField()),
                ('digest', models.CharField(max_length=200, null=True)),
                ('state', models.SlugField(default=b'AR', max_length=2, choices=[(b'AR', b'AWAITING REVIEW'), (b'UR', b'UNDER REVIEW'), (b'RM', b'REVIEW By MD'), (b'FP', b'FALSE POSITIVE - DO NOT PROCESS'), (b'Q', b'CONFIRMED CASE, QUEUE FOR SENDING'), (b'S', b'SENT TO HEALTH DEPARTMENT'), (b'AS', b'AUTO-SENT TO HEALTH DEPARTMENT (NO REVIEW)')])),
                ('case', models.ForeignKey(to='vaers.Case')),
                ('provider', models.ForeignKey(to='emr.Provider')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Report_Sent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('report', models.TextField()),
                ('report_type', models.CharField(max_length=20, db_index=True)),
                ('case', models.ForeignKey(to='vaers.Case')),
                ('questionnaire', models.ForeignKey(to='vaers.Questionnaire')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Sender',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_added', models.DateField(db_index=True, null=True, blank=True)),
                ('provider', models.ForeignKey(verbose_name=b'Physician', blank=True, to='emr.Provider', null=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='case',
            name='adverse_events',
            field=models.ManyToManyField(to='vaers.AdverseEvent', db_index=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='case',
            name='immunizations',
            field=models.ManyToManyField(related_name=b'all_immunizations', to='emr.Immunization', db_index=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='case',
            name='patient',
            field=models.ForeignKey(related_name=b'vaers_cases', to='emr.Patient'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='case',
            name='prior_immunizations',
            field=models.ManyToManyField(related_name=b'prior_immunizations', to='emr.Immunization'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='adverseevent',
            name='content_type',
            field=models.ForeignKey(to='contenttypes.ContentType'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='adverseevent',
            name='patient',
            field=models.ForeignKey(to='emr.Patient'),
            preserve_default=True,
        ),
    ]
