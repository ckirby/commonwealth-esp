# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.core.management import call_command


def load_fixture(apps, schema_editor):
    call_command('loaddata', 'data', app_label='vaers')

class Migration(migrations.Migration):

    dependencies = [
        ('vaers', '0001_initial'),
    ]

    operations = [
        migrations.RunPython(load_fixture),
    ]
