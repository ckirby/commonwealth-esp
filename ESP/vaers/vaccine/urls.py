from django.conf.urls import url
import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^manufacturers$', views.manufacturers),
    url(r'^vaccine/(?P<id>\d*)$', views.vaccine_detail),
    url(r'^manufacturer/(?P<id>\d*)$', views.manufacturer_detail)
    
]
