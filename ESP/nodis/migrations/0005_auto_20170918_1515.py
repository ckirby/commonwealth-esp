# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nodis', '0004_auto_20170918_1419'),
    ]

    operations = [
        migrations.AlterField(
            model_name='casereport',
            name='initial_lab',
            field=models.ForeignKey(blank=True, to='emr.LabResult', null=True),
        ),
    ]
