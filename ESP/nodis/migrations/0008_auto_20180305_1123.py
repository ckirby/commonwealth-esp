# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nodis', '0007_auto_20171018_1415'),
    ]

    operations = [
        migrations.AlterField(
            model_name='caseactivehistory',
            name='change_reason',
            field=models.CharField(max_length=8, choices=[(b'Q', b'Q - Qualifying events'), (b'D', b'D - Disqualifying events'), (b'E', b'E - Elapsed time')]),
        ),
        migrations.AlterField(
            model_name='caseactivehistory',
            name='status',
            field=models.CharField(max_length=8, choices=[(b'I', b'I - Initial detection and activation'), (b'R', b'R - Reactivated'), (b'D', b'D - Deactivated')]),
        ),
    ]
