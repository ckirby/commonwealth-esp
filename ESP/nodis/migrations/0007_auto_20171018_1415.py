# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nodis', '0006_auto_20170919_1049'),
    ]

    operations = [
        migrations.AddField(
            model_name='casereport',
            name='na_5_true_sent',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='casereport',
            name='na_trmt_true_sent',
            field=models.BooleanField(default=False),
        ),
    ]
