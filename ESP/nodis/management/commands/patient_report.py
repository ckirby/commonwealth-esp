#!/usr/bin/env python
'''
                                  ESP Health
                         Notifiable Diseases Framework
                           Patient Report Generator

Given a patient's medical record number (MRN), prints all patient's labs, 
encounters, prescriptions, heuristic events, and disease cases.
'''


import sys

from django.core.management.base import BaseCommand

from ESP.emr.models import Patient
from ESP.emr.models import LabResult
from ESP.emr.models import Encounter
from ESP.emr.models import Prescription
from ESP.nodis.models import Case


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-m', action='store_true', dest='mrn', default=False,
                            help='Lookup patient by MRN')
        parser.add_argument('-d', action='store_true', dest='dbid', default=False,
                            help='Lookup patient by database ID number')
        parser.add_argument('-s', '--summary', action='store_true', dest='summary',
                            default=False,  help='Print summary of cases for each patient')
        parser.add_argument('--phi', action='store_true', dest='phi', default=False, help='Include PHI in report')

    def handle(self, *args, **options):
        if not (options['dbid'] or options['mrn']):
            print "Arguments Missing. See 'patient_report --help'"
            sys.exit()
        if options['summary']:
            values = {'mrn': 'MRN', 'name': 'NAME', 'condition': 'CONDITION', 'date': 'DATE', 'id': 'Patient #'}
            print '-' * 80
            if options['phi']:
                print '%(mrn)-14s    %(name)-25s    %(condition)-15s    %(date)-10s' % values
            else:
                print '%(id)-10s    %(condition)-15s    %(date)-10s' % values
            print '-' * 80

        for identifier in options['dbid']:
            try:
                patient = Patient.objects.get(pk=identifier)
            except Patient.DoesNotExist:
                print >> sys.stderr, 'ERROR:  No patient found with database ID # %s.' % identifier
                continue
            self.write_patient_data(patient=patient, options=options)

        for identifier in options['mrn']:
            try:
                patient = Patient.objects.get(mrn=identifier)
            except Patient.DoesNotExist:
                print >> sys.stderr, 'ERROR:  No patient found with MRN "%s".' % identifier
                continue
            self.write_patient_data(patient=patient, options=options)


    def write_patient_data(self, patient, options):
        if options['summary']:
            self.patient_summary(patient=patient, options=options)
        else:
            self.patient_report(patient=patient, options=options)


    def patient_report(self, patient, options):
        print '~' * 80
        print '~' + ' ' * 78 + '~'
        print '~' + 'PATIENT REPORT'.center(78) + '~'
        print '~' + ' ' * 78 + '~'
        line = 'Patient #: %s' % patient.pk
        line = '~' + line.center(78) + '~'
        print line
        if options['phi']: # Print patient identification info at top of report
            line = 'MRN: %s' % patient.mrn
            line = '~' + line.center(78) + '~'
            print line
            line = 'NAME: %s' % patient.name
            line = '~' + line.center(78) + '~'
            print line # Print patient's MRN at top of report
        print '~' + ' ' * 78 + '~'
        print '~' * 80
        models_to_report = [  # Order in which info will be printed
            Case,
            Encounter,
            LabResult,
            Prescription,
            ]
        titles = {
            Case: 'CASES',
            Encounter: 'ENCOUNTERS',
            LabResult: 'LAB RESULTS',
            Prescription: 'PRESCRIPTIONS',
            }
        for model in models_to_report:
            objects = model.objects.filter(patient=patient).order_by('-date')
            if objects:
                print
                print '=' * 80
                print titles[model].center(80)
                print '=' * 80
                print model.str_line_header()
                print '-' * 80
            for obj in objects:
                print obj.str_line()
        print # Spacing before next report
        print
        print

    def patient_summary(self, patient, options):
        cases = patient.case_set.all()
        for c in cases:
            values = {'id': patient.pk, 'condition': c.condition, 'date': c.date}
            if options['phi']:
                values['mrn'] = patient.mrn
                values['name'] = patient.name
                print '%(mrn)-14s    %(name)-25s    %(condition)-15s    %(date)-10s' % values
            else:
                print '%(id)-10s    %(condition)-15s    %(date)-10s' % values
        if not cases:
            values = {'id': patient.pk, 'condition': 'NONE', 'date': '-'}
            if options['phi']:
                values['mrn'] = patient.mrn
                values['name'] = patient.name
                print '%(mrn)-14s    %(name)-25s    %(condition)-15s    %(date)-10s' % values
            else:
                print '%(id)-10s    %(condition)-15s    %(date)-10s' % values

